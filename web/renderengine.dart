/*The MIT License (MIT)

Copyright (c) 2013 Jens Herrmann <jens.herrmann@pinkeye.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/part of snakeboxapp;

class RenderEngineClass{
  CanvasElement canvas;
  CanvasRenderingContext2D context; 
  double fpsAverage;
  int w = 0;
  int h = 0;
  //Lets save the cell width in a variable for easy control
  int cw;
  int ch;
  num renderTime;

  
  int cells = 0;
  RenderEngineClass(this.canvas){  
    fpsAverage = 0.0;
    
  }

/**
 * Display the animation's FPS in a div.
 */
void showFps(num fps) {
  if (fpsAverage == null) {
    fpsAverage = fps;
  }
  fpsAverage = fps * 0.05 + fpsAverage * 0.95;

}

void update(){
  num time = new  DateTime.now().millisecondsSinceEpoch;
  
  if (renderTime != null) {
    showFps((1000 / (time - renderTime)).round());
    //ticks = (time - renderTime) / 1000.0f;
  }
  
  renderTime = time;

}

void draw(){
  query("#notes").text = "${fpsAverage.round().toInt()} fps";
}
void init(int fieldSize){
  this.context = canvas.getContext("2d"); 
  w = canvas.width;
  h = canvas.height;
  //Init default cellsize
  cw = (w ~/ fieldSize);
  ch = (h ~/ fieldSize);
  
  cells = fieldSize;
  renderTime = 0; 

}
void paint_cell(int pos, String color){
    this.context.fillStyle = "white";
    this.context.fillRect((pos % cells) * cw,  (pos ~/ cells) * ch, cw, ch);

    this.context.fillStyle = color;   
    this.context.fillRect((pos % cells) * cw,  (pos ~/ cells) * ch, cw, ch);
    this.context.strokeStyle = "white"; 
    this.context.strokeRect((pos % cells)  * cw, (pos ~/ cells) * ch, cw, ch);
    
}

void clear(){
  this.context.clearRect(0, 0, w, h);
}


}
