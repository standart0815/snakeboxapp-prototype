/*The MIT License (MIT)

Copyright (c) 2013 Jens Herrmann <jens.herrmann@pinkeye.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

library snakeboxapp;

import 'dart:html';
import 'dart:async';
import 'dart:json' as json;
import 'dart:collection';
import 'dart:math';
import 'dart:core';

part 'inputengine.dart';
part 'menuengine.dart';
part 'renderengine.dart';
part 'wsengine.dart';
part 'item.dart';
part 'stage.dart';
part 'snake.dart';


InputEngineClass input ;
MenuEngineClass menu;
GameEngineClass game;
RenderEngineClass renderer;
RenderEngineClass bgRenderer;
WsEngineClass host;

void main() {
  //Initilazing all the different handler
  //They are able to see eachother
  input = new InputEngineClass();
  menu = new MenuEngineClass();
  renderer = new RenderEngineClass(query('#gamecanvas'));
  bgRenderer = new RenderEngineClass(query('#bgcanvas'));
  game = new GameEngineClass();
  host = new WsEngineClass();

}
 

/**
 * Position des Spielers 
 * 
 */


class GameEngineClass {
  
  //Gamevariable
  String mode = "intro";  //TODO enum
  bool directioned = false;
  bool collisioned = false;

  int fieldSize = 50; 
  //related to Snake
  String d; //Direction
  int animationFrame;   //TODO:WIR NOCH BENOETIGT?
  
  Item fruit;
  Stage field;
  Snake player;
 
  
  GameEngineClass(){
    //init visibiliy
     menu.init();

 
  }  
  
  void init(){
    menu.load();
    //Init Canvas
    renderer.init(fieldSize);
    bgRenderer.init(fieldSize);
    //Init Inputhandler
    input.init();
    //Init Stage
    field = new Stage(renderer,{'x': fieldSize,'y': fieldSize}); 
    field.init();
    field.draw();
   
    //Init fruit
    fruit = new Item(renderer, "255, 0, 0", {'x': fieldSize,'y': fieldSize}, 500); 
    fruit.init();
    //Init snake
    player = new Snake(renderer, "180, 151, 245", "Jens", 3);
    player.init();
   
    //close starting screen
    menu.ingame();
    mode = "run";
    //go into render loop
    start(singleplayer: true);
    
  }

  //Renderloop
  void requestRedraw({bool singleplayer}) {
    if(singleplayer == true){
      window.requestAnimationFrame(draw);
    }
    else{
        
        //window.requestAnimationFrame(drawmp);
    }
  }
  

  void quit(){
    //SpielVariablen
    mode = "intro";  //TODO enum
    bool directioned = false;
    bool collisioned = false;

    int fieldSize = 50; 
    //Variablen fuer Schlange
    animationFrame = null;   //TODO:WIR NOCH BENOETIGT?
    
    fruit = null;
    field = null;
    player = null;
    
    renderer.clear();
  }
  

  void draw(num _) {
    //  print("Draw");
    //Was needed for pause screen, which is deactivated for the Protype
    if(mode == "paused"){
      mode = "run";
    
    
    }
    else {
 
      
    field.update();
    fruit.update();
  
    //if(host.joined && !host.directioned){
      
    //}
    
    //if(host.joined && host.directioned){
      
    //}
    //else{
      
    //
    
   
    //check latest userinput
    if(player.ticked(renderer.renderTime)){
      processInput();
      player.update();
    }
  
    renderer.update();
    menu.setScore(player.score, player.highscore);
    
    //draw all elements
    fruit.draw();
    player.draw();
    renderer.draw();
    
  

    requestRedraw(singleplayer: true);
    }
  }
  
  //is not used in the Protoype
  void quitlevel(){
    mode = "run";
    input.resetstates();
    menu.init();
  }
  //is not used in the Protoype
  void pause(bool paused){
    if(paused){
      mode = "paused";
      input.resetstates();
      menu.pause();
    }
    else{
      mode = "run";
      input.resetstates();
      menu.ingame();
      requestRedraw(singleplayer: true);
    }
  }
 
  
  //is not used in the Protoype
  bool connect(){
    return true;
  }
  
  void start({bool singleplayer}){
    requestRedraw(singleplayer: singleplayer);
  }
  

//collision detection
void processInput(){

 if(!player.collisioned){
  if (input.islastState('move-right')) player.setDirection('move-right');
  else if (input.islastState('move-left')) player.setDirection('move-left');
  //else if (input.islastState('move-up'))  player.setDirection('move-up');
  //else if (input.islastState('move-down')) player.setDirection('move-down');
  else if (input.islastState('pause')) pause(true);
  
  if(player.direction == 'move-right') player.head['x']++;
  else if(player.direction == 'move-left') player.head['x']--;
  else if(player.direction == 'move-up') player.head['y']--;
  else if(player.direction == 'move-down') player.head['y']++;
  
 }
 //left stage?
  if (field.collision(player.head)) {
    input.resetstates();
    player.collisioned = true;
  }
  //touched fruit?
  else if(fruit.collision(player.head)){
    player.ateFruit();
    fruit.eaten();
    
  }
  //touched snake?
  else if(player.collision(player.head)){ 
    if(input.lastState() != "none"){
         player.collisioned = true;
         input.resetstates();
    }
  }
  else{
      if(input.lastState() != "none" ||player.direction != "none")
           player.moved = true;
      }
      input.resetstates(); 
  }
 } 

 
 
 

