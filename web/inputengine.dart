/*The MIT License (MIT)

Copyright (c) 2013 Jens Herrmann <jens.herrmann@pinkeye.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/part of snakeboxapp;
/** TODO::
 * Integrate Mouse and Touch Events
 */


class InputEngineClass{

  // A dictionary mapping ASCII key codes to string values
  // describing the action we want to take when that key is
  // pressed.
  Map<String, String> _bindings;
  
  // A dictionary mapping actions that might be taken in our
  // game to a boolean value indicating whether that action
  // is currently being performed.
  Map<String, bool> _actions;
  
  Map mouse = {
    'x': 0,
    'y': 0
  };
  
  Map touch = {
               'x': 0,
               'y': 0,
               'state': false
  };

  
  String _lastaction;
  
  InputEngineClass(){
    _bindings = new Map();
    _actions = new Map();
    _lastaction = "none";
  }
  
  bool initialized = false;
  
  
  //-----------------------------
  void init() {
    if  (initialized)
        return;
    // Example usage of bind, where we're setting up
    // the W, A, S, and D keys in that order.
    this.bind(87, 'move-up');
    this.bind(65, 'move-left');
    this.bind(83, 'move-down');
    this.bind(68, 'move-right');
    this.bind(27, 'pause');
   
    // Adding the event listeners for the appropriate DOM events.
    //canvas.addEventListener('mousemove', this.onMouseMove);
    
    //new KeyboardEventController.keydown(document.window).add(onKeyDown); 
    //new KeyboardEventController.keyup(document.window).add(onKeyUp); 
    //new MouseEventController.but
    //new KeyboardEventController.keydown(document.window).add(onKeyDown);
    window.onKeyDown.listen((KeyboardEvent e) {
      // If the key is not set yet, set it with a timestamp.
     onKeyDown(e.keyCode);
    
    });
    
    window.onTouchStart.listen((TouchEvent e){
      onTochDown(e);
    });
    
    window.onTouchStart.listen((TouchEvent e){
      onTochUp(e);
    });


    window.onKeyUp.listen((KeyboardEvent e) {
      onKeyUp(e.keyCode);
    });
    
    initialized = true;
    
    
  }

  //----------------------------
  void onMouseMove(event) {
    this.mouse['x'] = event.clientX;
    this.mouse['y'] = event.clientY;
  }

  //-----------------------------
  void onKeyDown(int code){
    // Grab the keyID property of the event object parameter,
    // then set the equivalent element in the 'actions' object
    // to true.
    // 
    // You'll need to use the bindings object you set in 'bind'
    // in order to do this.
    var action = this._bindings[code];
 
    if (action != null) {
      _lastaction = action;     
      this._actions[action] = true;
    }
  }

  //-----------------------------
  void onKeyUp(int code) {
    // Grab the keyID property of the event object parameter,
    // then set the equivalent element in the 'actions' object
    // to false.
    // 
    // You'll need to use the bindings object you set in 'bind'
    // in order to do this.
    var action = this._bindings[code];

    if (action != null) {
      print("UP:: $action");
      this._actions[action] = false;
    }
  }
  
  void onTouchDown(TouchEvent e){
    // Grab the keyID property of the event object parameter,
    // then set the equivalent element in the 'actions' object
    // to true.
    // 
    // You'll need to use the bindings object you set in 'bind'
    // in order to do this.
   
  }

  //-----------------------------
  void onTouchUp(int x, int y) {
    // Grab the keyID property of the event object parameter,
    // then set the equivalent element in the 'actions' object
    // to false.
    // 
    // You'll need to use the bindings object you set in 'bind'
    // in order to do this.
    var action = this._bindings[code];

    if (action != null) {
      print("UP:: $action");
      this._actions[action] = false;
    }
  }


  // The bind function takes an ASCII keycode
  // and a string representing the act  ion to
  // take when that key is pressed.
  // 
  // Fill in the bind function so that it
  // sets the element at the 'key'th value
  // of the 'bindings' object to be the
  // provided 'action'.
  void bind(key, action) {
    this._bindings[key] = action;
  }
  
  bool state(action){
  
    if(this._actions[action] != null) 
        return this._actions[action];
    else 
        return false;
  }
 
  
  bool islastState(String action){
    if(action == _lastaction){
      return true;
    }
    else{
      return false;
    }
  }
  
  String lastState(){
    return _lastaction;
  }
  
  String setLastState(String lastAction){
    this._lastaction = lastAction;
  }
  
  //TODO:: Besere Metode forech finden... 
  void resetstates(){
    _lastaction = "none";
    _actions.forEach(resetElement);
  }
  
  void resetElement(key, value) {
    _actions[key] = false;
  
  }

}


