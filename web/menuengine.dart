/*The MIT License (MIT)

Copyright (c) 2013 Jens Herrmann <jens.herrmann@pinkeye.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/part of snakeboxapp;


class MenuEngineClass{
  
  bool _started = false;
  MenuEngineClass(){
    
  }
  void init(){
   
   // intro();
     main();
     
    
    //init DOM handler TODO:: In menuengine verschieben
     window.onClick.listen((mouseEvent) {
      if(_started == false){
          game.init(); // remain subscribed for all clicks
          _started = true;
      }
    });
   
  
//    
//    query('#playbutton_sp').onTouchStart.listen((mouseEvent) {
//      game.init(); // remain subscribed for all clicks
//    });
//    
//    query('#playbutton_mp').onClick.listen((mouseEvent) {
//      //loadmp(); // remain subscribed for all clicks
//    });
//    
//    query('#playbutton_mp').onTouchStart.listen((mouseEvent) {
//      //loadmp(); // remain subscribed for all clicks
//    });
//    
//    
//    query('#settingsbutton').onClick.listen((mouseEvent) {
//     setting(); // remain subscribed for all clicks
//    });
//    
//    query('#settingsbutton').onTouchStart.listen((mouseEvent) {
//      setting(); // remain subscribed for all clicks
//    });
//    
//    
//    query('#highscorebutton').onClick.listen((mouseEvent) {
//      highscore(); // remain subscribed for all clicks
//    });
//    
//    query('#highscorebutton').onTouchStart.listen((mouseEvent) {
//      highscore(); // remain subscribed for all clicks
//    });
//    
//    query('#return').onClick.listen((mouseEvent) {
//      game.pause(false); // remain subscribed for all clicks
//    });
//    
//    query('#return').onTouchStart.listen((mouseEvent) {
//      game.pause(false); // remain subscribed for all clicks
//    });
//    
//    query('#quitlevel').onClick.listen((mouseEvent) {
//     quit(); // remain subscribed for all clicks
//    });
//    
//    query('#quitlevel').onTouchStart.listen((mouseEvent) {
//      quit(); // remain subscribed for all clicks
//    });
//    
//    query('#quityes').onClick.listen((mouseEvent) {
//      game.quit(); // remain subscribed for all clicks
//      main();
//      
//    });
//    
//    query('#quityes').onTouchStart.listen((mouseEvent) {
//      game.quit(); // remain subscribed for all clicks
//      main();
//      
//    });
//    
//    query('#quitno').onClick.listen((mouseEvent) {
//      pause(); // remain subscribed for all clicks
//    });
//    
//    query('#quitno').onTouchStart.listen((mouseEvent) {
//      pause(); // remain subscribed for all clicks
//    });
//    
//    query('#left').onTouchStart.listen((mouseEvent) {
//      input.setLastState("move-left"); // remain subscribed for all clicks
//    });
//    
//    query('#right').onTouchStart.listen((mouseEvent) {
//      input.setLastState("move-right");// remain subscribed for all clicks
//    });
    
    
  }
//  NOT USED DUE PROTOTYPE
//  void intro(){
//    queryAll('.gamelayer').forEach((element){
//      element.hidden = true;
//    });
//    
//   // query('#intoscreen').hidden = false ;
//
//  }
  
  
  void main(){
    queryAll('.gamelayer').forEach((element){
      element.hidden = true;
    });
    
    query('#startinfo').hidden = false ;
  }
  
  void load(){
    queryAll('.gamelayer').forEach((element){
      element.hidden = true;
    });
    query('#loadingscreen').hidden = false;
    query('#loadingmessage').hidden = false; 
  }
  
  void ingame(){  
    queryAll('.gamelayer').forEach((element){
      element.hidden = true;
     });
  
    // Display the game canvas and score 
    query('#gamecanvas').hidden = false;
    query('#scorescreen').hidden = false;
   
  }
  
  void setScore(int score, int highscore){
    query('#scorescreen').text = "Score: ${score} /${highscore}";
  }
  

  
  void quit(){
    query('#loadingscreen').hidden = false;
    query('#loadingmessage').hidden = true; 
    query('#pausescreen').hidden = true;
    query('#endingscreen').hidden = false;

  
  }
  
  void pause(){
    query('#loadingscreen').hidden = false;
    query('#loadingmessage').hidden = true; 
    query('#pausescreen').hidden = false;
    query('#endingscreen').hidden = true;

  }
  
  void setting() {
    query('#loadingscreen').hidden = false;
    query('#loadingmessage').hidden = true; 
    query('#settingsscreen').hidden = false;
  
  }
  
  void highscore(){
    query('#loadingscreen').hidden = false;
    query('#loadingmessage').hidden = true; 
    query('#scorescreen').hidden = false;

    
  }
   
  
}

