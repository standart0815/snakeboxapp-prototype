/*The MIT License (MIT)

Copyright (c) 2013 Jens Herrmann <jens.herrmann@pinkeye.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/part of snakeboxapp;


//Not used in the prototype 
//Was used for muliplayer
class WsEngineClass{
  Map map = new Map();
  //Variabken fuer MP Server Connection
  const String IP = "localhost:8000/ws"; //"193.164.131.248:16000/ws/";
  WebSocket ws;
  bool joined = false;
  bool directioned = false;
  var wsc;
  
  WsEngineClass(){
    map["join"] = {"_type" : "join", "attributes": {"deviceID" : "2", "PlayerName": "Sven"}};
    map["joined"] = {"_type" : "joied", "attributes": {"color" : "180, 151, 245", "length": 3}};
    map["disconnect"] =  {"_type" : "disconnect", "attributes": {"deviceID" : "2"}};
    map["position"] =  {"_type" : "position", "attributes": {"x" : "0", "y": "0"}};
    map["direction"] =  {"_type" : "direction", "attributes": {"direction" : "right"}};
    map["collision"] = {"_type" : "collision", "attributes": {"x" : "0", "y": "0"}};
  }
  
  void init(){
        
        ws = new WebSocket("ws://$IP");
    
       
        ws.onOpen.listen((e) {
           ws.send(json.stringify(this.map["join"]));
        });
    
        ws.onMessage.listen((MessageEvent evt){
           
            var response = json.parse(evt.data);
          
            if(response["_type"] == "joined" && !joined) {
                start(response);
             }
             else if(response["_type"] == "updated" && joined == true){
                //directioned = true;   
                //field = response["attributes"]["field"];
            }
            else if(response["_type"] == "directioned" && joined == true){  
                
            }  
            else if(response["_type"] == "collisioned" && joined == true){
             //  d = "right";
              //  position.x = response["attributes"]["x"];
             //   position.y = response["attributes"]["y"];
       
            }
      }); 
    
    
    
  }
  
  void start(var response){
    joined = true;
  }
  
  
  
}
 

