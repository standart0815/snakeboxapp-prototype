/*The MIT License (MIT)

Copyright (c) 2013 Jens Herrmann <jens.herrmann@pinkeye.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/part of snakeboxapp;

class Stage{
   Map range = {'x': 0, 'y':0};
   RenderEngineClass rend;
   Stage( this.rend, this.range){ 
   }
    void init(){
     
   }
  
   void update(){
    
   }
  
   bool collision(Map pos){
    if(pos['x'] <= -1 || pos['x'] >= range['x'] || pos['y'] <= -1 || pos['y'] >= range['y']){
      return true;
    }
    else {
      return false;
    }
   }
  
   void draw(){
    this.rend.context.fillStyle = "white";
    this.rend.context.fillRect(0, 0,  renderer.w,  renderer.h);
   // this.rend.context.strokeStyle = "black";
   // this.rend.context.strokeRect(0, 0,  renderer.w,  renderer.h);
  }
}

