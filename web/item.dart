/*The MIT License (MIT)

Copyright (c) 2013 Jens Herrmann <jens.herrmann@pinkeye.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/ part of snakeboxapp;

class Item{
  

  int position;
  int last; //last postion of the item
  int lifecycle;
  int lifespan;
  Map range = {'x': 0, 'y':0};
  int cooldown;
  bool alive;
  String color;
  Random rng = new Random();
  
  double delta;
  double alpha;
  
  RenderEngineClass renderer;
  
 Item(this.renderer, this.color, this.range, this.lifecycle,[String sprite]){
   alive = false; 
   lifespan = 0;
   delta = alpha = 0.0;
   cooldown = 30;
   
   
 }
 
 void init(){
   alive = true;
   lifespan = lifecycle;
   alpha = 1.0;
   delta = 1.0/lifespan;
   respostion();
 }
 
 int respostion([int xpos, int ypos]){
   last = position;
   if(xpos!= null && ypos != null){
 
      position = (ypos * range['y]']) + xpos;
   }
   else{
     position = rng.nextInt(range['x'] * range['y'] - 1);
 
   } 
   return position;
 }
 
 void update(){
   lifespan -= 1;
   alpha  -= delta;
   if(lifespan <= 0 && alive == true){
     alive = false;
     lifespan = rng.nextInt(cooldown);
   }
   if(lifespan <= 0 && alive == false) {
     init();     
   }
 }
 
 void draw(){
   if(last != null){
     this.renderer.paint_cell(this.last, "white");
     last = null;
   }
   this.renderer.paint_cell(this.position, "rgba( $color, ${(alpha+0.1)})");
 
 }
 
 void eaten(){
   this.respostion();
 }
 
 bool collision(Map pos){
   if( ((pos['y'] * renderer.cells) + pos['x']) == position){
         this.alive = false;
         return true;
   }
   else {
     return false;
   }
 } 
}




