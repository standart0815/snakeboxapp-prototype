/*The MIT License (MIT)

Copyright (c) 2013 Jens Herrmann <jens.herrmann@pinkeye.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/part of snakeboxapp;

class Snake{
 String owner;
 String color;
 Map head = {'x': 25, 'y':25};
 Queue<int> tail;

 
 int last;
 int length = 60;
 
 bool alive; 
 bool collisioned;
 
 bool moved;
 Random rng = new Random();
 
 double alpha;
 double delta;
 
 int lastRefresh;
 int tick;
 
 int highscore;
 int score;
 
 RenderEngineClass renderer;
 
 String direction;
 
 Snake( this.renderer, this.color, this.owner,[ this.length]){
   alive = true;
   collisioned = false;
   score = 0;
   highscore = 0;
   
   moved = false;
   tail = new Queue();
   alpha = 0.0;
   lastRefresh = 0;
   tick = 100;
   
   score = 0;
   direction = "none";
 
 }
 
 void init(){
   head['x'] = head['x'] + length;
   head['y'] = head['y'];
   for(int i = 0; i < length; i++){
     tail.add(head['y'] - i);
   }
   
   length = 3;
 }
 
 Map respostion([int xpos, int ypos, bool resurrect]){
   if(xpos!= null && ypos != null){
     if(xpos < length){
      head['x'] = xpos + length;
     }
     else {
       head['x'] = xpos;
     }
      head['y'] = ypos;
      for(int i = 0; i < length; i++){
        tail.addLast(head['x'] - i);
      }
   }
   else{
     head['x'] = rng.nextInt(game.field.range['x']);
     head['y'] = rng.nextInt(game.field.range['y']);
   } 
   
   if(resurrect){
     score = 0;
     tick = 100;
     length = 3;
     alive = true;
     collisioned = false;
     alpha = 0.0;
   }
   return head;
 }
 
 void update(){
   if(collisioned){
      if(tail.isEmpty){
          kill();
      }
      else{       
        last = tail.removeFirst(); 
      }
   }
   else{   
   if(moved == true){
       moved = false;
       score += 1;
       tail.add((head['y'] * renderer.cells) + head['x']);       
       if(tail.length > length) {
         last = tail.removeFirst(); 
       }
   }
      
    if(score > highscore)
        highscore =score;
    
  }
   
 }
 
 void kill(){
    respostion(null, null , true);
 }
 
 void paint_cell(element){
   this.renderer.paint_cell(element,"rgba( $color, ${(alpha+0.1)})");  
   alpha += delta;
 }
 
 bool collision(Map pos){
     return tail.contains((pos['y'] * renderer.cells) + pos['x']);
 }
 
 void setDirection(String direction){
   if(direction == 'move-right'){
     if(this.direction == 'move-right')
       this.direction = 'move-down';
     else if (this.direction == 'move-up')
       this.direction = 'move-right';
     else if (this.direction == 'move-down')
       this.direction = 'move-left';
     else if (this.direction == 'move-left')
       this.direction = 'move-up';
     else
       this.direction = direction;
     
     
   }     
   else if(direction == 'move-left'){
     if(this.direction == 'move-right')
       this.direction = 'move-up';
     else if (this.direction == 'move-up')
       this.direction = 'move-left';
     else if (this.direction == 'move-down')
       this.direction = 'move-right';
     else if (this.direction == 'move-left')
       this.direction = 'move-down';
     else
       this.direction = direction;
   }
   else
      this.direction = direction;
 }
 
 
 
 void draw(){
   delta = 0.7 / length;
   alpha = 0.0;
   if(last != null){
     this.renderer.paint_cell(last,"rgba( 255,255,255,100)");  
     last = null;
   }
   if(alive == true){ 
      tail.forEach(paint_cell);
   }
 }
 
 bool ticked(int timer){
   if((timer - lastRefresh ) > tick){
     lastRefresh = timer;
     return true;    
   }
   else {
     return false;
   }
 } 
 
 void ateFruit(){
    this.length++;
    score +=  (110- tick); 
    if(tick >= 20){
      tick -= 5;
    }
  }
   
}


