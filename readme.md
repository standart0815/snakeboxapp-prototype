﻿# SnakeBoxApp Prototype

Try the [SnakeBoxApp Prototype ](http://www.pinkeye.de/SnakeBoxApp_Prototype/snakeboxapp.html).

## Info
This is the Prototype of the Snake clone SnakeBoxApp published by Pinkeye Games.
It is written in Dart and released under the MIT license.

* Play the Game [SnakeBoxApp Prototype ](http://www.pinkeye.de/SnakeBoxApp_Prototype/snakeboxapp.html).
* Checkout the source: `git clone https://standart0815@bitbucket.org/standart0815/snakeboxapp-prototype.git` and install it yourself.

   
## Structure
File overview:

* **snakeboxapp.dart**  - Main game loop in which movement and game logic is procesed
* **snake.dart** - Entry of the players snake. Repositioning, tail length, and Collision is handled here.
* **item.dart** - Entry of the fruits.
* **inputengine.dart** - The InputEnige handles the different input states and buffers them. 
* **menuengine.dart** - MenuEngineClass menu Handles all ingame menus (titelscreen, pausescreen) 
* ***renderengine.dart** - Renders all the different objects into the canvas the draw method is called by the snake and item objects 
* **wsengine.dat** - Handler for the Multiplayer communication between clients and server (not implemented in the prototype) 


## Contact
Jens Herrmann ([jens.herrmann@pinkeye.de](mailto:jens.herrmann@pinkeye.de))

Pinkeye Games ([http://www.pinkeye.de](http://www.pinkeye.de))


## Credits

Copyright: ©2013,  Jens Herrmann, [http://jens.herrmann.pinkeye.de](http://jens.herrmann.pinkeye.de)

License: [http://opensource.org/licenses/MIT](http://opensource.org/licenses/MIT) MIT License (MIT)
